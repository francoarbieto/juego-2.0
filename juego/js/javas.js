var canvas = document.getElementById("milienzo");
var contexto = canvas.getContext("2d");

var fondo = new Image();
var personaje = new Image();
var tacho_n = new Image();
var tacho_b = new Image();
var tacho_v = new Image();
var tacho_a = new Image();
var basura_n = new Image();
var basura_b = new Image();
var basura_v = new Image();
var basura_a = new Image();
var vidas = new Image();
var vacio = new Image();
var agua = new Image();

agua.src = "img/agua.jpg";
vidas.src = "img/completo.png";
vacio.src = "img/incompleto.png";
fondo.src= "img/fondo.png";
personaje.src= "img/parado_derecha.png";
tacho_n.src= "img/negro.png";
tacho_b.src=  "img/blanco.png";
tacho_v.src= "img/verde.png";
tacho_a.src= "img/azul.png";
basura_n.src = "";
basura_b.src = "";
basura_v.src = "";
basura_a.src = "";

var tiempo = 30;
var velocidad = 0.02;
var nivel = 1;
var gravedad = 12;
var basura_t = 50;
var basura_o = "ninguna";
/*
(tipo,forma,x,y,visibilidad)
tipos: 
1.- organico
2.- plastico
3.- vidrio
4.- papel
*/
var b_n = [["img/sandia.png"],["img/huevo.png"],["img/pez.png"],["img/pollo.png"]];
var b_b = [["img/botella_p.png"],["img/botellita.png"],["img/botellota_p.png"],["img/cuchara.png"]];
var b_v = [["img/botella.png"],["img/botellota.png"],["img/vidrio.png"]];
var b_a = [["img/carta.png"],["img/carton.png"],["img/frugos.png"],["img/periodico.png"]];
var basuras = [[1,Math.floor(Math.random()*4),Math.floor(Math.random()*8)*150,500,true],[2,Math.floor(Math.random()*4),Math.floor(Math.random()*8)*150,500,true],[3,Math.floor(Math.random()*4),Math.floor(Math.random()*8)*150,500,true],[4,Math.floor(Math.random()*4),Math.floor(Math.random()*8)*150,500,true]];

var tacho_n_x = 350;
var tacho_n_y = 487;

var tacho_b_x = 500;
var tacho_b_y = 487;

var tacho_v_x = 650;
var tacho_v_y = 487;

var tacho_a_x = 800;
var tacho_a_y = 487;

var tacho_t = 90;

var personaje_x=60;
var personaje_y=300;
var personaje_t_x = 180;
var personaje_t_y = 180;
var direction = "derecha";
var maximo = 100;
var rapidez = 5;
var salto= false;
var ya_salto = false;
var puede_saltar = true;
var puntos = 0;
var salud = 3;
var daniado = 0;
var ya_n = false;
var ya_b = false;
var ya_v = false;
var ya_a = false;
var cambio = 0;
var avanzando = false;
var recoger = false;

	document.addEventListener("keydown",mover);
	function mover (){
		if(daniado == 0)
		{
		if (event.keyCode == 37){
			personaje.src = "img/corriendo_i.png";
			direction = "izquierda";
			avanzando = true;
		}
		if (event.keyCode == 39){
			personaje.src = "img/corriendo_d.png";
			direction = "derecha";
			avanzando = true;
		}	
		if (event.keyCode == 38){
			if (personaje_y == 400 && puede_saltar == true)
			salto = true;	
			puede_saltar = false;
		}
		if (event.keyCode == 40){
			recoger = true;
		}
	}
	}
	document.addEventListener("keyup",detenerse);
	function detenerse (){
		if (daniado == 0){
		if (event.keyCode == 37){
			if (direction == "izquierda"){
			personaje.src = "img/parado_izquierda.png";
			//direction = "ninguno";
			avanzando = false;
			}
			}
		if (event.keyCode == 39){
			if (direction == "derecha"){
			personaje.src = "img/parado_derecha.png";
			//direction = "ninguno";
			avanzando = false;
			}
			}
		if (event.keyCode == 38){
			puede_saltar = true;
			salto = false;
		}
		if (event.keyCode == 40){
			recoger = false;
		}
		}
	}



function dibujar(){
	if (daniado > 0){
		avanzando = false;
		if (direction == "izquierda")
		{
			personaje.src = "img/golpeado_izquierda.png"
		}
		if (direction == "derecha")
		{
			personaje.src = "img/golpeado_derecha.png"
		}
		daniado += 1;
	}
	if (daniado == 30){
		daniado = 0;
	}
	if (daniado == 0){
	if (avanzando ==true){
		cambio += 1;
	}else{
		cambio = 0;
	}
	if (cambio == 20){
		cambio = 0;
	}
		if (tiempo < 0){
			tiempo = 0;
		}

		if (direction == "izquierda" && personaje_y != 400)
			if (salto == false){
			personaje.src = "img/cayendo_izquierda.png";
		}else{
			personaje.src = "img/saltando_izquierda.png";
		}
		if (direction == "derecha" && personaje_y != 400)
			if (salto == false){
			personaje.src = "img/cayendo_derecha.png"; 
		}else{
			personaje.src = "img/saltando_derecha.png";
		}
		
		if (avanzando == false){
		if (direction == "izquierda" && personaje_y == 400)
			personaje.src = "img/parado_izquierda.png";
		if (direction == "derecha" && personaje_y == 400)
			personaje.src = "img/parado_derecha.png";				
			}
		if (avanzando == true){
		if (direction == "izquierda" && personaje_y == 400){
			if (cambio < 10 && cambio > 0){		
			personaje.src = "img/corriendo_izquierda.png";
		}else{
			personaje.src = "img/parado_izquierda.png";
		} 
		}
		if (direction == "derecha" && personaje_y == 400){
			if (cambio < 10 && cambio > 0){	
			personaje.src = "img/corriendo_derecha.png";				
			}else{
				personaje.src = "img/parado_derecha.png";
		}
		}
	}

	if (collision_n() == true && basura_o == "ninguna"){
		basura_o = "organico";
	}
	if (basura_o == "organico"){
		basuras[0][2] = personaje_x+(personaje_t_x - basura_t)/2;
		basuras[0][3] = personaje_y - 96; 
		if (ya_n == false){
			if(collision_t_n() == true)
				basuras[0][2] = tacho_n_x+(tacho_t - basura_t)/2;
			if(collision_t_b() == true)
				basuras[0][2] = tacho_b_x+(tacho_t - basura_t)/2;
			if(collision_t_v() == true)
				basuras[0][2] = tacho_v_x+(tacho_t - basura_t)/2;
			if(collision_t_a() == true)
				basuras[0][2] = tacho_a_x+(tacho_t - basura_t)/2;
			if(collision_t_n() == true){
			basura_o = "ninguna";
			puntos += 20;
			basuras[0][3] = 300;
			//basuras[0][2] = tacho_n_x+(tacho_t - basura_t)/2;
			ya_n = true;
		}
		if(collision_t_b() == true || collision_t_v() == true || collision_t_a() == true){
			daniado = 1;
			basura_o = "ninguna";
			puntos -= 5;
			basuras[0][3] = 300;
			//basuras[0][2] = tacho_n_x+(tacho_t - basura_t)/2;
			ya_n = true;
			salud-= 1;
			}
		}
	}
	if (collision_b() == true && basura_o == "ninguna"){
		basura_o = "plastico";
	}
	if (basura_o == "plastico"){
		basuras[1][2] = personaje_x+(personaje_t_x - basura_t)/2;
		basuras[1][3] = personaje_y - 64; 
		if (ya_b == false){
			if(collision_t_n() == true)
				basuras[1][2] = tacho_n_x+(tacho_t - basura_t)/2;
			if(collision_t_b() == true)
				basuras[1][2] = tacho_b_x+(tacho_t - basura_t)/2;
			if(collision_t_v() == true)
				basuras[1][2] = tacho_v_x+(tacho_t - basura_t)/2;
			if(collision_t_a() == true)
				basuras[1][2] = tacho_a_x+(tacho_t - basura_t)/2;
			if(collision_t_b() == true){
			basura_o = "ninguna";
			puntos += 20;
			basuras[1][3] = 300;
			//basuras[1][2] = tacho_b_x+(tacho_t - basura_t)/2;
			ya_b = true;
			}
			if(collision_t_n() == true || collision_t_v() == true || collision_t_a() == true){
			daniado = 1;
			basura_o = "ninguna";
			puntos -= 5;
			basuras[1][3] = 300;
			//basuras[1][2] = tacho_b_x+(tacho_t - basura_t)/2;
			ya_b = true;
			salud-= 1;
			}
		}
	}
	if (collision_v() == true && basura_o == "ninguna"){
		basura_o = "vidrio";
	}
	if (basura_o == "vidrio"){
		basuras[2][2] = personaje_x+(personaje_t_x - basura_t)/2;
		basuras[2][3] = personaje_y - 64; 
		if (ya_v == false){
			if(collision_t_n() == true)
				basuras[2][2] = tacho_n_x+(tacho_t - basura_t)/2;
			if(collision_t_b() == true)
				basuras[2][2] = tacho_b_x+(tacho_t - basura_t)/2;
			if(collision_t_v() == true)
				basuras[2][2] = tacho_v_x+(tacho_t - basura_t)/2;
			if(collision_t_a() == true)
				basuras[2][2] = tacho_a_x+(tacho_t - basura_t)/2;
			if (collision_t_v() == true){
			basura_o = "ninguna";
			puntos += 20;
			basuras[2][3] = 300;
			//basuras[2][2] = tacho_v_x+(tacho_t - basura_t)/2;
			ya_v = true;
			}
			if (collision_t_n() == true || collision_t_b() == true || collision_t_a() == true){
			daniado = 1;
			basura_o = "ninguna";
			puntos -= 5;
			basuras[2][3] = 300;
			//basuras[2][2] = tacho_v_x+(tacho_t - basura_t)/2;
			ya_v = true;
			salud-= 1;
			}
		}
	}
	if (collision_a() == true && basura_o == "ninguna"){
		basura_o = "papel o cartón";
	}
	if (basura_o == "papel o cartón"){
		basuras[3][2] = personaje_x+(personaje_t_x - basura_t)/2;
		basuras[3][3] = personaje_y - 64; 
		if (ya_a == false){
			if(collision_t_n() == true)
				basuras[3][2] = tacho_n_x+(tacho_t - basura_t)/2;
			if(collision_t_b() == true)
				basuras[3][2] = tacho_b_x+(tacho_t - basura_t)/2;
			if(collision_t_v() == true)
				basuras[3][2] = tacho_v_x+(tacho_t - basura_t)/2;
			if(collision_t_a() == true)
				basuras[3][2] = tacho_a_x+(tacho_t - basura_t)/2;
			if (collision_t_a() == true){
			basura_o = "ninguna";
			puntos += 20;
			basuras[3][3] = 300;
			//basuras[3][2] = tacho_a_x+(tacho_t - basura_t)/2;
			ya_a = true;
			}
			if (collision_t_n() == true || collision_t_b() == true || collision_t_v() == true){
			daniado = 1;
			basura_o = "ninguna";
			puntos -= 5;
			basuras[3][3] = 300;
			//basuras[3][2] = tacho_a_x+(tacho_t - basura_t)/2;
			ya_a = true;
			salud-= 1;
			}
		}
	}
	if (ya_n == true && ya_b == true && ya_v == true && ya_a == true || salud == 0){
		if (salud == 0){
			nivel -= 1;
			salud = 3;
		}
		tiempo = 30;
		nivel += 1;
		velocidad = 0.02;
		personaje_x=60;
		personaje_y=300;
		cambio = 0;
		daniado = 0;
		b_n = [["img/sandia.png"],["img/huevo.png"],["img/pez.png"],["img/pollo.png"]];
		b_b = [["img/botella_p.png"],["img/botellita.png"],["img/botellota_p.png"],["img/cuchara.png"]];
		b_v = [["img/botella.png"],["img/botellota.png"],["img/vidrio.png"]];
		b_a = [["img/carta.png"],["img/carton.png"],["img/frugos.png"],["img/periodico.png"]];
		basuras = [[1,Math.floor(Math.random()*4),Math.floor(Math.random()*7)*150,500,true],[2,Math.floor(Math.random()*4),Math.floor(Math.random()*7)*150,500,true],[3,Math.floor(Math.random()*4),Math.floor(Math.random()*7)*150,500,true],[4,Math.floor(Math.random()*4),Math.floor(Math.random()*7)*150,500,true]];
		ya_n = false;
		ya_b = false;
		ya_v = false;
		ya_a = false;

	}
}
	
	function mal (){
		daniado = 1;
		salud -= 1;

	}

	function collision_n (){
			if (personaje_x>basuras[0][2]+basura_t-64 || personaje_x+personaje_t_x < basuras[0][2]+64 || personaje_y > basuras[0][3]+basura_t || personaje_y+personaje_t_y < basuras[0][3]){
				return false;
			}else{
			if (ya_n == false && recoger == true)
			return true;
		}
		}
	function collision_b (){
			if (personaje_x>basuras[1][2]+basura_t-64 || personaje_x+personaje_t_x < basuras[1][2]+64 || personaje_y > basuras[1][3]+basura_t || personaje_y+personaje_t_y < basuras[1][3]){
				return false;
			}else{
			if (ya_b == false && recoger == true)
			return true;
		}
		}
	function collision_v (){
			if (personaje_x>basuras[2][2]+basura_t-64 || personaje_x+personaje_t_x < basuras[2][2]+64 || personaje_y > basuras[2][3]+basura_t || personaje_y+personaje_t_y < basuras[2][3]){
				return false;
			}else{
			if (ya_v == false && recoger == true)
			return true;
		}
		}
	function collision_a (){
			if (personaje_x>basuras[3][2]+basura_t-64 || personaje_x+personaje_t_x < basuras[3][2]+64 || personaje_y > basuras[3][3]+basura_t || personaje_y+personaje_t_y < basuras[3][3]){
				return false;
			}else{
			if (ya_a == false && recoger == true)
			return true;
		}
		}	

	function collision_t_n (){
			if (personaje_x>tacho_n_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_n_x+96 || personaje_y > tacho_n_y+tacho_t || personaje_y+personaje_t_y < tacho_n_y){
				return false;
			}else{
			return true;
		}
		}	
	function collision_t_b (){
			if (personaje_x>tacho_b_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_b_x+96 || personaje_y > tacho_b_y+tacho_t || personaje_y+personaje_t_y < tacho_b_y){
				return false;
			}else{
			return true;
		}
		}	
	function collision_t_v (){
			if (personaje_x>tacho_v_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_v_x+96 || personaje_y > tacho_v_y+tacho_t || personaje_y+personaje_t_y < tacho_v_y){
				return false;
			}else{
			return true;
		}
		}	
	function collision_t_a (){
			if (personaje_x>tacho_a_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_a_x+96 || personaje_y > tacho_a_y+tacho_t || personaje_y+personaje_t_y < tacho_a_y){
				return false;
			}else{
			return true;
		}
		}
	function collision (){
		if (nivel > 4)
		if (personaje_x>850 && personaje_y+personaje_t_y > 320){
				return true;
			}else{
			return false;
		}
	}	
	if (collision() == true){
		rapidez = 3;
		maximo = 320;
	}else{
			maximo = 100;
			rapidez = 5;
	}
	basuras[0][3] += 12;
	
	if (basuras[0][3] >= 500){
		basuras[0][3] = 500;
	}
	basuras[1][3] += 12;
	
	if (basuras[1][3] >= 500){
		basuras[1][3] = 500;
	}
	basuras[2][3] += 12;
	
	if (basuras[2][3] >= 500){
		basuras[2][3] = 500;
	}
	basuras[3][3] += 12;
	
	if (basuras[3][3] >= 500){
		basuras[3][3] = 500;
	}

	personaje_y += gravedad;
	
	if (personaje_y >= 400){
		personaje_y = 400;
	}

	if (direction=="izquierda" && avanzando == true)
		personaje_x-=rapidez;
	if (direction=="derecha" && avanzando == true)
		personaje_x+=rapidez;
	if (salto==true){
		personaje_y-=30;
	}
	if (personaje_y <= maximo){
		salto = false;
	}
	//Se dibuja fondo
	contexto.drawImage(fondo,0,-50,1200,700);
	//Se dibujan puntos, vidas y elementos
	contexto.font = "bold 25px verdana";
	contexto.fillText("Puntos: "+puntos+"pts",500,40);
	contexto.fillText("Basura: "+basura_o,500,70);
	contexto.fillText("Nivel: "+nivel,900,40);
	contexto.fillText("Tiempo: "+Math.floor(tiempo)+" seg",900,70);
	//contexto.fillText("salto: "+salto,90,400);
	//contexto.fillText("daniado:"+daniado,personaje_x,personaje_y-32);
	contexto.fillText("Vidas:",70,70)
	//contexto.fillText("puede saltar: "+puede_saltar,200,600);
	for (var i = 0; i < 3; i++) {
		contexto.drawImage(vacio,170+i*30,40,40,40);
	}
	for (var i = 0; i < salud; i++) {
		contexto.drawImage(vidas,170+i*30,40,40,40);
	}

	//Se dibujan personajes y elementos
	if (nivel > 4){
		for (var i = 0; i < 24; i++){
		contexto.drawImage(agua,1150-i*50,550+i/8*50,50,200);
		}
	}
	//contexto.fillText([basuras[0][2]],300,300);
	basura_n.src = b_n[basuras[0][1]];
	contexto.drawImage(basura_n,basuras[0][2],basuras[0][3],basura_t,basura_t);
	basura_b.src = b_b[basuras[1][1]];
	contexto.drawImage(basura_b,basuras[1][2],basuras[1][3],basura_t,basura_t);
	basura_v.src = b_v[basuras[2][1]];
	contexto.drawImage(basura_v,basuras[2][2],basuras[2][3],basura_t,basura_t);
	basura_a.src = b_a[basuras[3][1]];
	contexto.drawImage(basura_a,basuras[3][2],basuras[3][3],basura_t,basura_t);
	contexto.drawImage(tacho_n,tacho_n_x,tacho_n_y,tacho_t,tacho_t);
	contexto.drawImage(tacho_b,tacho_b_x,tacho_b_y,tacho_t,tacho_t);
	contexto.drawImage(tacho_v,tacho_v_x,tacho_v_y,tacho_t,tacho_t);
	contexto.drawImage(tacho_a,tacho_a_x,tacho_a_y,tacho_t,tacho_t);
	//contexto.drawImage(basura_n,basura_n_x,basura_n_y,basura_t,basura_t);
	//contexto.drawImage(basura_b,basura_b_x,basura_b_y,basura_t,basura_t);
	//contexto.drawImage(basura_v,basura_v_x,basura_v_y,basura_t,basura_t);
	//contexto.drawImage(basura_a,basura_a_x,basura_a_y,basura_t,basura_t);

	contexto.drawImage(personaje,personaje_x,personaje_y,personaje_t_x,personaje_t_y);
	tiempo = tiempo-(nivel*velocidad/4+0.015);
	requestAnimationFrame(dibujar);
}
dibujar();