var canvas = document.getElementById("milienzo");
var contexto = canvas.getContext("2d");

var fondo= new Image();
var personaje = new Image();
var tacho_n = new Image();
var tacho_b = new Image();
var tacho_v = new Image();
var tacho_a = new Image();
var basura_n = new Image();
var basura_b = new Image();
var basura_v = new Image();
var basura_a = new Image();

fondo.src= "img/ciudad.jpg";
personaje.src= "img/parado_derecha.png";
tacho_n.src= "img/negro.png";
tacho_b.src=  "img/blanco.png";
tacho_v.src= "img/verde.png";
tacho_a.src= "img/azul.png";
basura_n.src = "img/organico.jpg";
basura_b.src = "img/plastico.jpg";
basura_v.src = "img/vidrio.jpg";
basura_a.src = "img/papel.jpg";

var basura_n_x = 500;
var basura_n_y = 520;

var basura_b_x = 700;
var basura_b_y = 520;

var basura_v_x = 300;
var basura_v_y = 520;

var basura_a_x = 900;
var basura_a_y = 520;

var basura_t = 60;

var basuras_n = 0;
var basuras_b = 0;
var basuras_v = 0;
var basuras_a = 0; 

var basura_o = "ninguna";

var tacho_n_x = 400;
var tacho_n_y = 487;

var tacho_b_x = 500;
var tacho_b_y = 487;

var tacho_v_x = 600;
var tacho_v_y = 487;

var tacho_a_x = 700;
var tacho_a_y = 487;

var tacho_t = 88;

var personaje_x=60;
var personaje_y=300;
var personaje_t_x = 180;
var personaje_t_y = 180;
var direction = "ninguno";
var maximo = personaje_y-200;

var salto= false;
var ya_salto = false;
var puede_saltar = true;
var puntos = 32;

var ya_n = false;
var ya_b = false;
var ya_v = false;
var ya_a = false;

	document.addEventListener("keydown",mover);
	function mover (){
		if (event.keyCode == 37){
			personaje.src = "img/corriendo_i.png";
			direction = "izquierda";
		}
		if (event.keyCode == 39){
			personaje.src = "img/corriendo_d.png";
			direction = "derecha";
		}	
		if (event.keyCode == 38){
			if (personaje_y == 400 && puede_saltar == true)
			salto = true;	
			puede_saltar = false;
		}
	}
	document.addEventListener("keyup",detenerse);
	function detenerse (){
		if (event.keyCode == 37){
			if (direction == "izquierda"){
			personaje.src = "img/parado_izquierda.png";
			direction = "ninguno";
			}
			}
		if (event.keyCode == 39){
			if (direction == "derecha"){
			personaje.src = "img/parado_derecha.png";
			direction = "ninguno";
			}
			}
		if (event.keyCode == 38){
			puede_saltar = true;
			salto = false;
		}
		}



function dibujar(){
	//if (puntos >= 100){
	//	document.script = "js/javas.js"; 
	//}
	if (collision_n() == true && basura_o == "ninguna"){
		basura_o = "organicas";
	}
	if (basura_o == "organicas"){
		basura_n_x = personaje_x+(personaje_t_x - basura_t)/2;
		basura_n_y = personaje_y - 96; 
		if (collision_t_n() == true){
			basura_o = "ninguna";
			puntos += 20;
			basura_n_y = 800;
			ya_n = true;
		}
	}
	if (collision_b() == true && basura_o == "ninguna"){
		basura_o = "plastico";
	}
	if (basura_o == "plastico"){
		basura_b_x = personaje_x+(personaje_t_x - basura_t)/2;
		basura_b_y = personaje_y - 64; 
		if (collision_t_b() == true){
			basura_o = "ninguna";
			puntos += 20;
			basura_b_y = 800;
			ya_b = true;
		}
	}
	if (collision_v() == true && basura_o == "ninguna"){
		basura_o = "vidrio";
	}
	if (basura_o == "vidrio"){
		basura_v_x = personaje_x+(personaje_t_x - basura_t)/2;
		basura_v_y = personaje_y - 64; 
		if (collision_t_v() == true){
			basura_o = "ninguna";
			puntos += 20;
			basura_v_y = 800;
			ya_v = true;
		}
	}
	if (collision_a() == true && basura_o == "ninguna"){
		basura_o = "papel";
	}
	if (basura_o == "papel"){
		basura_a_x = personaje_x+(personaje_t_x - basura_t)/2;
		basura_a_y = personaje_y - 64; 
		if (collision_t_a() == true){
			basura_o = "ninguna";
			puntos += 20;
			basura_a_y = 800;
			ya_a = true;
		}
	}
	function collision_n (){
			if (personaje_x>basura_n_x+basura_t-64 || personaje_x+personaje_t_x < basura_n_x+64 || personaje_y > basura_n_y+basura_t || personaje_y+personaje_t_y < basura_n_y){
				return false;
			}else{
			return true;
		}
		}
	function collision_b (){
			if (personaje_x>basura_b_x+basura_t-64 || personaje_x+personaje_t_x < basura_b_x+64 || personaje_y > basura_b_y+basura_t || personaje_y+personaje_t_y < basura_b_y){
				return false;
			}else{
			return true;
		}
		}
	function collision_v (){
			if (personaje_x>basura_v_x+basura_t-64 || personaje_x+personaje_t_x < basura_v_x+64 || personaje_y > basura_v_y+basura_t || personaje_y+personaje_t_y < basura_v_y){
				return false;
			}else{
			return true;
		}
		}
	function collision_a (){
			if (personaje_x>basura_a_x+basura_t-64 || personaje_x+personaje_t_x < basura_a_x+64 || personaje_y > basura_a_y+basura_t || personaje_y+personaje_t_y < basura_a_y){
				return false;
			}else{
			return true;
		}
		}	

	function collision_t_n (){
			if (personaje_x>tacho_n_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_n_x+96 || personaje_y > tacho_n_y+tacho_t || personaje_y+personaje_t_y < tacho_n_y){
				return false;
			}else{
			return true;
		}
		}	
	function collision_t_b (){
			if (personaje_x>tacho_b_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_b_x+96 || personaje_y > tacho_b_y+tacho_t || personaje_y+personaje_t_y < tacho_b_y){
				return false;
			}else{
			return true;
		}
		}	
	function collision_t_v (){
			if (personaje_x>tacho_v_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_v_x+96 || personaje_y > tacho_v_y+tacho_t || personaje_y+personaje_t_y < tacho_v_y){
				return false;
			}else{
			return true;
		}
		}	
	function collision_t_a (){
			if (personaje_x>tacho_a_x+tacho_t-96 || personaje_x+personaje_t_x < tacho_a_x+96 || personaje_y > tacho_a_y+tacho_t || personaje_y+personaje_t_y < tacho_a_y){
				return false;
			}else{
			return true;
		}
		}	

	personaje_y += 15;
	
	if (personaje_y >= 400){
		personaje_y = 400;
	}
	basura_n_y += 15;
	basura_b_y += 15;
	basura_v_y += 15;
	basura_a_y += 15;
	
	if (basura_n_y >= 500 && ya_n == false){
		basura_n_y = 500;
	}
	if (basura_b_y >= 500 && ya_b == false){
		basura_b_y = 500;
	}
	if (basura_v_y >= 500 && ya_v == false){
		basura_v_y = 500;
	}
	if (basura_a_y >= 500 && ya_a == false){
		basura_a_y = 500;
	}

	if (direction=="izquierda")
		personaje_x-=5;
	if (direction=="derecha")
		personaje_x+=5;
	if (salto==true){
		personaje_y-=30;
	}
	if (personaje_y <= maximo){
		salto = false;
	}
	contexto.drawImage(fondo,0,0,1200,600);
	//contexto.drawImage(tacho_n,tacho_n_x,tacho_n_y,tacho_t,tacho_t);
	//contexto.drawImage(tacho_b,tacho_b_x,tacho_b_y,tacho_t,tacho_t);
	contexto.drawImage(tacho_v,tacho_v_x,tacho_v_y,tacho_t,tacho_t);
	contexto.drawImage(tacho_a,tacho_a_x,tacho_a_y,tacho_t,tacho_t);
	contexto.drawImage(basura_n,basura_n_x,basura_n_y,basura_t,basura_t);
	contexto.drawImage(basura_b,basura_b_x,basura_b_y,basura_t,basura_t);
	contexto.drawImage(basura_v,basura_v_x,basura_v_y,basura_t,basura_t);
	contexto.drawImage(basura_a,basura_a_x,basura_a_y,basura_t,basura_t);

	contexto.drawImage(personaje,personaje_x,personaje_y,personaje_t_x,personaje_t_y);
	contexto.fillText("Puntos: "+puntos+"pts",10,20);
	contexto.fillText("basuras orgánicas: "+basura_o,90,20);
	contexto.fillText("salto: "+salto,90,400);
	contexto.fillText("puede saltar: "+puede_saltar,90,600);
	requestAnimationFrame(dibujar);
}
dibujar();